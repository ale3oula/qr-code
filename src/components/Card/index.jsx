import React from 'react'
import QRcode from '../Qrcode';
import styled from 'styled-components'

const Container = styled.div`
    background-color: var(--white);
    border-radius: 16px;
    max-width: 320px;
    padding: 16px;
    box-shadow: 0 25px 25px 0 rgba(0, 0, 0, 0.04);
`;

const TextContainer = styled.div`
    margin-top: 16px;
    text-align: center;
`

const Title = styled.h1`
    font-size: 22px;
    line-height: 1.1em;
    text-overflow: ellipsis;
    width: 100%;
    color: var(--dark-blue);
    margin-bottom: 16px;
`;

const Paragraph = styled.p`
    font-size: 15px;
    color: var(--grayish-blue);
    padding: 16px;
`;

export default function Card() {
  return (
    <Container>
        <QRcode />
        <TextContainer>
            <Title>Improve your front-end skills by building projects</Title>
            <Paragraph>Scan the QR code to visit Frontend Mentor and take your coding skills to the next level</Paragraph>
        </TextContainer>
    </Container>
  )
}
