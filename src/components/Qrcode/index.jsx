import React from "react";
import QRImage from "./image-qr-code.png";
import styled from "styled-components";

const Image = styled.img`
  border-radius: 16px;
`;

// const API =
  // "https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=Example";

export default function QRcode() {
  return (
    <div>
      <Image alt="QR code" src={QRImage} width={288} height={288} />
      {/* <img src={API} alt="QR Code" width={300} height={300} /> */}
    </div>
  );
}
