import Card from "./components/Card";
import styled from 'styled-components'

const Wrapper = styled.div`
  display: grid;
  place-items: center;

  height: 100vh;
`

function App() {
  return (
    <Wrapper>
      <Card />
    </Wrapper>
  );
}

export default App;
